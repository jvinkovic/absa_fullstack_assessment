﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ABSA_Phonebook_Backend.Interfaces;
using ABSA_Phonebook_Backend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace ABSA_Phonebook_Backend.Controllers
{
    /// <summary>
    /// The contact controller handles all the requests
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly IContactRepository _contactRepository;

        /// <summary>
        /// Constructor used to assign an IContactRepository instance
        /// </summary>
        /// <param name="contactRepository">New/Existing IContactRepository instance</param>
        public ContactController(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;
        }

        /// <summary>
        /// Fetch all contacts
        /// </summary>
        /// <returns>List-CContact</returns>
        [HttpGet]
        public async Task<ActionResult> GetContacts()
        {
            try
            {
                //Fetch all documents from repo and await result
                var items = await _contactRepository.GetAll();
                return Ok(items.OrderBy(x => x.contactName));
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Fetch all contacts and filter by selected phonebook
        /// </summary>
        /// <param name="id">String Phonebook entry id</param>
        /// <returns>List-CContact</returns>
        [HttpGet]
        [Route("byPhonebook/id/{id}")]
        public async Task<ActionResult> GetContactsByPhoneBook(string id)
        {
            try
            {
                //Fetch all documents from repo and await result then check the id
                var contacts = await _contactRepository.GetAll();
                var contactList = contacts.Where(c => c.phonebookId == id);

                return Ok(contactList);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Fetch all contacts and filter by selected phonebook and search string
        /// </summary>
        /// <param name="searchString">STRING search phrase</param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("byPhonebook/{searchString}/{id}")]
        public async Task<ActionResult> GetContactsBySearchPhonebook(string searchString, string id)
        {
            try
            {
                var contacts = await _contactRepository.GetAll();
                var contactList = contacts.Where(c => c.phonebookId == id & c.contactName.ToLower().Contains(searchString.ToLower()));
                return Ok(contactList);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }


        /// <summary>
        /// Fetch all contacts and filter by search string
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{searchString}")]
        public async Task<ActionResult> GetContactsBySearch(string searchString)
        {
            try
            {
                var contacts = await _contactRepository.GetAll();
                return Ok(contacts.Where(c =>
                    c.contactName.ToLower().Contains(searchString.ToLower())));
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Creating a new Contact entry
        /// </summary>
        /// <param name="newContact">JSON New Contact entry document</param>
        [HttpPost]
        public async Task<ActionResult> CreateContact([FromBody] CContact newContact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var contact = await _contactRepository.Create(newContact);

            return Ok(contact);
        }

        /// <summary>
        /// Updating an existing Contact entry
        /// </summary>
        /// <param name="contactEntry">JSON Contact entry document</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateContact([FromBody] CContact contactEntry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            await _contactRepository.Update(contactEntry);

            return Ok();
        }

        /// <summary>
        /// Deleting a Contact entry
        /// </summary>
        /// <param name="id">JSON Contact entry document</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult> DeleteContact(string id)
        {
            try
            {
                var contacts = await _contactRepository.GetAll();
                var contact = contacts.Where(p => p.Id == id).AsEnumerable().Single();

                var contactToDelete = new CContact() { Id = contact.Id, phonebookId = contact.phonebookId, contactName = contact.contactName, phoneNumber = contact.phoneNumber};

                await _contactRepository.Delete(contactToDelete);

                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}