﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABSA_Phonebook_Backend.Models;

namespace ABSA_Phonebook_Backend.Interfaces
{
    /// <summary>
    /// IContactRepository will be used when contact APIs implement functions that phonebooks doesn't.
    /// </summary>
    public interface IContactRepository : IRepository<CContact>
    {
    }
}
