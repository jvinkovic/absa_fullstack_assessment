﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSA_Phonebook_Backend.Services
{
    /// <summary>
    /// Interface for cosmos db factory
    /// </summary>
    public interface ICosmosDbFactory
    {
        ICosmosDbService GetService(string collectionName);
    }
}
