﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ABSA_Phonebook_Backend.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace ABSA_Phonebook_Backend.Services
{
    /// <summary>
    /// CosmosDB Document CRUD functions
    /// </summary>
    public interface ICosmosDbService
    { 
       
        Task<Document> CreateDocument(object document, RequestOptions options = null, bool disableGUID = false, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<Document>> ReadDocuments(RequestOptions options = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<Document> UpdateDocument(object document, string docId, RequestOptions options = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<Document> DeleteDocument(string docId, RequestOptions options = null, CancellationToken cancellationToken = default(CancellationToken));

    }
}
