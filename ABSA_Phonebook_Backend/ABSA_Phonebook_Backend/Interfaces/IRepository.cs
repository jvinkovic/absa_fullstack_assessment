﻿using ABSA_Phonebook_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSA_Phonebook_Backend.Interfaces
{

    /// <summary>
    /// These are the basic API functions that both contacts and phonebooks must implement
    /// </summary>
    /// <typeparam name="T">Generic type</typeparam>
    public interface IRepository<T> where T : CEntry
    {
        Task<List<T>> GetAll();
        Task<T> Create(T entry);
        Task Update(T entry);
        Task Delete(T entry);
    }
}
