﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSA_Phonebook_Backend.Models
{

    /// <summary>
    /// Model used for all phonebook entries
    /// </summary>
    public class CPhonebook : CEntry
    {
        /// <summary>
        /// Phonebook entry name
        /// </summary>
        /// <example>BBD Contacts</example>
        public string name { get; set; }

        /// <summary>
        /// Phonebook entry description
        /// </summary>
        /// <example>BBD Cape Town contact list</example>
        public string description { get; set; }
    }
}
