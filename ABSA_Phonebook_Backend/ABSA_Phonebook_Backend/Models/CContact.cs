﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSA_Phonebook_Backend.Models
{

    /// <summary>
    /// Model used for all contact entries
    /// </summary>
    public class CContact: CEntry
    {
        /// <summary>
        /// Contact entry name
        /// </summary>
        /// <example>Morgan Freeman</example>
        public string contactName { get; set; }

        /// <summary>
        /// Contact entry Phone-number
        /// </summary>
        /// <example>(082) 855-6661</example>
        public string phoneNumber { get; set; }

        /// <summary>
        /// Contact entry phonebook origin
        /// </summary>
        /// <example>GUID</example>
        public string phonebookId { get; set; }
    }
}
