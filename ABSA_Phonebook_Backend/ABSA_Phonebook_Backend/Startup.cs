﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using ABSA_Phonebook_Backend.Interfaces;
using ABSA_Phonebook_Backend.Repository;
using ABSA_Phonebook_Backend.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;

namespace ABSA_Phonebook_Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           
            var connectionStringsOptions = Configuration.GetSection("ConnectionStrings").Get<CConnectionStringsOptions>();
            var cosmosDb = Configuration.GetSection("CosmosDb").Get<CCosmosDbOptions>();
            var (serviceEndpoint, authKey) = connectionStringsOptions.ActiveConnectionString;
            var (databaseName, collectionNames) = cosmosDb;
            var collectionNameInfo = collectionNames.Select(c => c.Name).ToList();


            //Dependency injection

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1).AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            services.AddCosmosDb(serviceEndpoint,authKey, databaseName, collectionNameInfo);


            services.AddScoped<IPhonebookRepository, CPhonebookRepository>();

            services.AddScoped<IContactRepository, CContactRepository>();


            //Populating and publishing swagger file URL/swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info {
                    Version = "v1",
                    Title = "ABSA Assessment-February 2019",
                    Description = "ABSA full stack assessment build in ASP.NET Core Web API",
                    Contact = new Contact
                    {
                        Name = "Arnold Hendricks",
                        Email = "arnold@bbd.co.za",
                    },
                    License = new License
                    {
                        Name = "Use under BBD",
                        Url = "https://www.bbd.co.za/"
                    }
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

           
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ABSA_Assessment V1");
            });

            app.UseHttpsRedirection();
            app.UseMvc();
            
        }
    }
}
