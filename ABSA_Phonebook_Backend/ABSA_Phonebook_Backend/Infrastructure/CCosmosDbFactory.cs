﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace ABSA_Phonebook_Backend.Services
{
    public class CCosmosDbFactory : ICosmosDbFactory
    {
        private readonly string databaseName;
        private readonly List<string> collectionNames;
        private readonly DocumentClient documentClient;

        /// <summary>
        /// Factory constructor create a new instance to connect to CosmosDb 
        /// </summary>
        /// <param name="_databaseName">e.g. phonebookDb</param>
        /// <param name="_collectionName">e.g. contacts</param>
        /// <param name="_documentClient">DocumentClient instance created by ServiceCollectionCosmosDB</param>
        public CCosmosDbFactory(string _databaseName, List<string> _collectionName, DocumentClient _documentClient)
        {
            databaseName = _databaseName ?? throw new ArgumentNullException(nameof(_databaseName));
            collectionNames = _collectionName ?? throw new ArgumentNullException(nameof(_collectionName));
            documentClient = _documentClient ?? throw new ArgumentNullException(nameof(_documentClient));
        }
        public ICosmosDbService GetService(string collectionName)
        {
            if (!collectionName.Contains(collectionName))
            {
                throw new ArgumentException($"Collection name, {collectionName}, does not exist.");
            }

            return new CosmosDbService(databaseName, collectionName, documentClient);
        }

        /// <summary>
        /// Test the validity of the CosmosDb setup
        /// </summary>
        public async Task CosmosDbSetupCheck()
        {
            await documentClient.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(databaseName));

            foreach (var name in collectionNames)
            {
                await documentClient.ReadDocumentCollectionAsync(
                    UriFactory.CreateDocumentCollectionUri(databaseName,name));
            }
        }
    }
}
