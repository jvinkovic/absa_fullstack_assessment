﻿using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSA_Phonebook_Backend.Services
{
    public static class ServiceCollectionCosmosDB
    {
        /// <summary>
        /// Create a new cosmosDb service collection
        /// </summary>
        /// <param name="services">Collection of services injected for the application</param>
        /// <param name="serviceEndpoint">CosmosDb URI</param>
        /// <param name="authKey">CosmosDb Primary Key</param>
        /// <param name="databaseName">CosmosDb Database name</param>
        /// <param name="collectionNames">List of collection names</param>
        /// <returns></returns>
        public static IServiceCollection AddCosmosDb(this IServiceCollection services, Uri serviceEndpoint,
            string authKey, string databaseName, List<string> collectionNames)
        {

            var documentClient = new DocumentClient(serviceEndpoint, authKey, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
            documentClient.OpenAsync().Wait();

            var cosmosDbFactory = new CCosmosDbFactory(databaseName, collectionNames, documentClient);
            cosmosDbFactory.CosmosDbSetupCheck().Wait();

            services.AddSingleton<ICosmosDbFactory>(cosmosDbFactory);

            return services;
        }
    }
}
