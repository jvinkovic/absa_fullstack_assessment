import { Component } from '@angular/core';
import { NavController ,NavParams, MenuController, AlertController, Nav, App, Events} from 'ionic-angular';
import {HttpProvider} from "../../providers/http/http";
import {Util} from "../../providers/util/util";
import { Phonebook } from '../../models/phonebook';
import { LoadingOverlay } from '../../providers/LoadingOverlay';
import { PhonebookProvider } from '../../providers/http/phonebook';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { FormPage } from '../form/form';
import { filterQueryId } from '@angular/core/src/view/util';
import { ContactPage } from '../contact/contact';
import { TabsPage } from '../tabs/tabs';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public Util = Util;
  private friends: any;
  private _phonebooks: Phonebook[] = [];
  private _avatar: any;
  private items: any;
  private searchQuery: string = '';


  constructor(public navCtrl: NavController,
     public navParams:NavParams,
     public menuCtrl: MenuController,
      public http:HttpProvider,
      private _alrtCtrl: AlertController,
      private _phonebookProvider: PhonebookProvider,
      private _loading: LoadingOverlay,
      private _app: App,
      private events: Events) {

   
  }

  ionViewDidLoad() {
    this.menuCtrl.enable(true);
    this.loadPhonebooks();
  }


  goToContacts(p: Phonebook){
  
     this.events.publish('change-tab',1,"ByPhonebook",p.id);
     
  }

  goToCreateForm(type: string) {
    this.navCtrl.push(FormPage, {"HomePage": this, "FormType": type, "_phonebooks": this._phonebooks});
  }

  goToEditForm(p: Phonebook) {
    this.navCtrl.push(FormPage, {"HomePage": this, "FormType": "Edit Phonebook", "_phonebook": p});
  }


  getPhonebook(ev: any) {
    // Reset items back to all of the items
    // set val to the value of the searchbar
    let val = ev.target.value;
    console.log(val);
   
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {

      this._phonebookProvider.getPhonebookBySearch(val).subscribe((phonebooks) => {
     
        this._phonebooks = phonebooks;
    
        this._phonebooks.forEach(phonebook => {
         phonebook.avatar = Util.pathAvatar();
        });

      })
    }
    else{
      this.loadPhonebooks();
    }
 
  }


  async loadPhonebooks(refresher?: any) {

    let popup = this._loading.ShowMessage('Loading...');

    this._phonebookProvider.getPhonebooks().subscribe((phonebooks) => {
     
    this._phonebooks = phonebooks;

    this._phonebooks.forEach(phonebook => {
     phonebook.avatar = Util.pathAvatar();
    });

    popup.dismiss();
      
    if (refresher)
    refresher.complete();
     });
  }

  

  delete(p: Phonebook) {

    let alert = this._alrtCtrl.create({
      title: 'Delete phonebook',
      message: 'Are you sure you want to delete this phonebook?',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {

            let popup = this._loading.ShowMessage('Loading...');
            
            this._phonebookProvider.deletePhonebook(p.id).subscribe(() =>{
              popup.dismiss();
              this.loadPhonebooks();
            },(err) =>{
                console.log(err);
            });

          }
        }
      ]
    });

    alert.present();

  }

  

}
