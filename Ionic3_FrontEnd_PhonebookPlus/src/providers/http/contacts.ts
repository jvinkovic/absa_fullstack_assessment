import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppSettingsProvider } from '../app-settings';
import { Observable } from 'rxjs/Observable';
import { Contact } from '../../models/contact';
import { Phonebook } from '../../models/phonebook';

/*
  Generated class for the DonationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ContactsProvider {

  constructor(private _http: HttpClient, private _appsettings: AppSettingsProvider) {

  }

 

getContactEntries(): Observable<Contact[]> { 
    return this._http.get<Contact[]>(`${this._appsettings.APIEndPoint}/contact`);
   }

getContactEntriesByPhonebook(id: string): Observable<Contact[]>{
    return this._http.get<Contact[]>(`${this._appsettings.APIEndPoint}/contact/byPhonebook/id/` + id);
}

getContactBySearch(name: string, pID: string): Observable<Contact[]> { 

  if(pID.length >= 2)
    return this._http.get<Contact[]>(`${this._appsettings.APIEndPoint}/contact/byPhonebook/` + name + '/' + pID);
  else 
    return this._http.get<Contact[]>(`${this._appsettings.APIEndPoint}/contact/` + name);
}

 createContact(c: Contact){
   return this._http.post(`${this._appsettings.APIEndPoint}/contact`,c);
 }

 deleteContact(id: string) { 
  return this._http.delete(`${this._appsettings.APIEndPoint}/contact/` + id);
}

updateContact(c: Contact) { 
  return this._http.put(`${this._appsettings.APIEndPoint}/contact`, c);
}

}