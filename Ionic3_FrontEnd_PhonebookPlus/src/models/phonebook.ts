export class Phonebook {
    id?: string;
    name?: string;
    description?: string;
    avatar?: string;
  }