import { NgModule, ErrorHandler, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { FormPage } from '../pages/form/form';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpProvider } from '../providers/http/http';
import { Util } from '../providers/util/util';
import { PhonebookProvider } from '../providers/http/phonebook';
import { ContactsProvider } from '../providers/http/contacts';
import { AppSettingsProvider } from '../providers/app-settings';
import { LoadingOverlay } from '../providers/LoadingOverlay';
import { HttpClientModule } from '@angular/common/http';

import { CallNumber } from '@ionic-native/call-number';

import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    FormPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TextMaskModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    FormPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpProvider,
    PhonebookProvider,
    ContactsProvider,
    AppSettingsProvider,
    LoadingOverlay,
    Util,
    CallNumber
  ]
})
export class AppModule {}
enableProdMode();